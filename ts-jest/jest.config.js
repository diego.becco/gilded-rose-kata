/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleDirectories: ['node_modules', 'core'],
  moduleNameMapper: {
    "^core/(.*)$": "<rootDir>/core/$1"
  }
};