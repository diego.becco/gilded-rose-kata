import Shop from "core/domain/Shop";
import Item from "core/domain/Item";

test("Gilded Rose", () => {
  let gildedRose = new Shop([new Item("foo", 0, 0)])
  const items = gildedRose.updateQuality()

  expect(items[0].name).toBe("foo")
})
