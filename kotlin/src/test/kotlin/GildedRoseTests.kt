import gildedrose.Example
import gildedrose.Item
import gildedrose.GildedRose
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.test.Ignore

class GildedRoseTests {
    @Test
    fun `Example test`(){
        val useCase = Example()

        val result = useCase.execute()

        result.shouldBe("Hello world!")
    }


}